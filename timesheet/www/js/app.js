// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


document.addEventListener("deviceready", function () {
	
	connectionStatus = navigator.onLine ? true : false;
});
angular.module('Timesheet', ['ionic', 'Timesheet.controllers','ngIOS9UIWebViewPatch']).run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		//alert("Document ready");
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}
	});

}).run(function($rootScope, CommonCode, $ionicPlatform) {
	//Handling back button click
	$rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
		CommonCode.showHeader(toState.name, true);
		if (toState.name == "ADD TIMELOG" || toState.name == "Login") {
			$ionicPlatform.registerBackButtonAction(function(event) {
				event.preventDefault();
			}, 100);
		}
	});
}).config(function($stateProvider, $urlRouterProvider, calendarConfigProvider) {

	$stateProvider.state('splash', {
		url : "/splash",
		templateUrl : "templates/splash.html",
		controller : 'SplashCtrl'
	}).state('Login', {
		cache : false,
		url : "/login",
		templateUrl : "templates/login.html",
		controller : 'LoginCtrl'
	}).state('Projects List', {
		cache : false,
		url : '/projects',
		templateUrl : "templates/projects.html",
		controller : 'ProjectsCtrl'
	}).state('Time Log', {
		cache : false,
		url : "/timelog",
		templateUrl : "templates/timelog.html",
		controller : 'TimelogCtrl'
	}).state('ADD TIMELOG', {
		cache : false,
		url : "/calendar",
		templateUrl : "templates/calendar.html",
		controller : 'CalendarCtrl'
	});
	calendarConfigProvider.setDateFormats({
		// hour: 'HH:mm' // this will configure times on the day view to display in 24 hour format rather than the default of 12 hour
		weekDay : 'EEE'
	});
	$urlRouterProvider.otherwise('/splash');
}).run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {

  //Change this to false to return accessory bar 
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
});
